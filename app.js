const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const router = express.Router();
const recipes = require('./public/recipes.json');
const recipesFeatured = require('./public/recipesFeatured.json');
const stories = require('./public/stories.json');
const storiesFeatured = require('./public/storiesFeatured.json')

const app = express();
const port = 3010;

app.use(cors());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

router.post('/recipes', (req, res) => {
  const { pageIndex, pageSize } = req.body;
  if (pageSize === -1) {
    res.send({ responseData: recipes });
  }
  const recipesResp = recipes.slice(pageSize * pageIndex, pageSize * (pageIndex + 1));
  res.send({ responseData: recipesResp });
});

router.post('/stories', (req, res) => {
  const { pageIndex, pageSize } = req.body;
  if (pageSize === -1) {
    res.send({ responseData: storiesFeatured });
  }
  const storiesResps = stories.slice(pageSize * pageIndex, pageSize * (pageIndex + 1));
  res.send({ responseData: storiesResps });
});

router.get(`/recipe/detail/:slug`, (req, res) => {
  const slug = req.params.slug;
  const recipeResp = recipes.find((recipe) => recipe.slug === slug);
  res.send(JSON.stringify({ responseData: recipeResp}));
});

router.get(`/story/detail/:slug`, (req, res) => {
  const slug = req.params.slug;
  const storyResp = stories.find((story) => story.slug === slug);
  res.send(JSON.stringify({ responseData: storyResp}));
});

app.use('/', router)

app.listen(port, () => {
  console.log('Express running');
})